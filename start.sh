#!/bin/bash

docker run --rm -p 9000:9000 -p 8000:8000 --name portainer \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v `pwd`/portainer/templates/templates.json:/templates.json \
  -v `pwd`/portainer/volume/data:/data \
  portainer/portainer:latest \
  --admin-password='$2y$05$oksAgWYOTy/E6vXaXseGUOAWK.cf6Tufnucdui87Efqh3/wvCzLaK' \
  --host unix:///var/run/docker.sock
